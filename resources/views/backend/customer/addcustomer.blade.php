@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Customer-care / Reservation Cancelation Officer</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storecustomercare') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="name"/>
                                        </div>
                                    </div>
                                    
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="image" value="{{ old('name') }}" required autofocus>
                                        </div>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Email</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="email"/>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Phone No.</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="mobile"/>
                                        </div>
                                    </div>

                                      <div class="form-group">
                                        <label class="col-md-2 control-label">Type</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="type">
                                            <option value="">Choose one</option>
                                               
                                            <option value="0">Customer care Officer</option>
                                            <option value="1">Reservation Cancelation Officer</option>
                                              
                                            </select>
                                        </div>
                                    </div> 

                                                                                
                                

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>


                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection
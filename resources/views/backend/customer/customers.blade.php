@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Employee</h3>
                                   <div class="pull-right">
                                       <a href="{{ url('addcustomercare') }}" class="btn btn-primary"><span class="fa fa-plus"></span>Add New Employee</a>
                                   </div>                           
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style="width: 10%">Name</th>
                                                <th>Employee Section</th>
                                                <th>Image</th>
                                                <th>Email</th>
                                                <th>Phone No.</th>
                                              
                                               
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($services as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->name }}</td>
                                                <td>
                                                    @if ($cat->type == 0)
                                                        Customer Care 
                                                    @else
                                                        Reservation Cancelation 
                                                    @endif
                                                </td>
                                                <td><img style="width: 100px; height: 60px" src="{{ asset('public/uploads/customer/'.$cat->image)  }}"></td>
                                                <td>{{ $cat->email }}</td>
                                                <td>{{ $cat->mobile }}</td>
                                                
                                    
                                                <td>
                                                   {{--  <a href="{{ url('editpartner/'.$cat->id) }}" class="btn btn-primary">Edit</a> --}}
                                                  

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deletecustomercare/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection
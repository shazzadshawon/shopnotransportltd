@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Reservation</h3>
                                  {{--  <div class="pull-right">
                                       <a href="{{ url('addlocation') }}" class="btn btn-primary"><span class="fa fa-plus"></span>Add Location</a>
                                   </div>    --}}                        
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Client Name</th>
                                                <th>Pick place</th>
                                                <th>Dropoff place</th>
                                                <th>Car</th>
                                                <th>Car Image</th>
                                               
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($reservations as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->first_name }} {{ $cat->last_name }}</td>
                                                <td>{{ $cat->pickup_location }}<br> {{ $cat->pick_up }}</td>
                                                <td>{{ $cat->dropoff_location }}<br> {{ $cat->drop_off }}</td>
                                                <td>{{ $cat->service_title }}</td>
                                                <td>
                                                @php
                                                    
                                                    $car = DB::table('item1')
                                                    ->where('id',$cat->car_id)
                                                    ->first();
                                                @endphp
                                                @if(!empty($car))
                                                    <img style="width: 100px" src="{{ url('public/uploads/item1/'.$car->image_name) }}">
                                                @endif
                                                </td>
                                               
                                                   {{--  <a href="{{ url('editpartner/'.$cat->id) }}" class="btn btn-primary">Edit</a> --}}
                                                  <td>

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deletecarreservation/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection
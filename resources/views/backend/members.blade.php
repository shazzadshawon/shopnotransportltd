@extends('layouts.backend')
@section('content')
<div class="col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Registered Customer List</h2>
            <div class="box-icon">
            
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Name</th>  
                        <th>Image</th>  
                        <th>Nid</th>  
                        <th>Mobile No.</th>  
                        <th>Email address</th>
                        <th>Facebook Profile Link</th>
                        <th>Address</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                   @php
                       $i=1;
                   @endphp
                    <?php
                    foreach ($members as $sub){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $sub->name; ?></td>
                        <td><img style="width: 100px; height: 100px" src="{{asset('public/uploads/registration/'.$sub->photo)}}"></td>
                        <td><?php echo $sub->nid; ?></td>
                        <td><?php echo $sub->telephone;?></td>
                        <td><?php echo $sub->email;?></td>
                        <td><?php echo $sub->fb;?></td>
                        <td><?php echo $sub->address;?></td>
                       
                        
                       
                        <td class="center">
                            
                           
                           
                            <a class="btn btn-danger" href="{{URL::to('/deletemember/'.$sub->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++; } ?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection
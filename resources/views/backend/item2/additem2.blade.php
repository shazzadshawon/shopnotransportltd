@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add New Service</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storeitem2') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Car</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="car_id">
                                                @foreach ($cars as $car)
                                                    <option value="{{ $car->id }}">{{ $car->service_title  }} - {{ $car->service_title_bn}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Place</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="place"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Place (in bangla)</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="place_bn"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Note</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="note"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Note (in bangla)</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="note"/>
                                        </div>
                                    </div>

                                
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Duration </label>
                                        <div class="col-md-10">
                                            <div class="">
                                               <input type="text" class="form-control" name="duration"/>
                                            </div>
                                        </div>
                                    </div>
                                               
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Duration (in bangla) </label>
                                        <div class="col-md-10">
                                            <div class="">
                                               <input type="text" class="form-control" name="duration_bn"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Cost</label>
                                        <div class="col-md-10">
                                            <div class="">
                                               <input type="text" class="form-control" name="cost"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Cost (in bangla) </label>
                                        <div class="col-md-10">
                                            <div class="">
                                               <input type="text" class="form-control" name="cost_bn"/>
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection
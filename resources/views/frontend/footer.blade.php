<footer>
    <div class="container">
        <div class="footer_links" align="center">
            <a href="#top" class="scroll-to"><img  src="{{url('public/frontend/img/logo_shopno.png')}}" class="img-responsive" alt="Shopno|Transport"></a>
            <a class="facebook" target="_blank" href="https://www.facebook.com/kotkoti.transport"><i class="fa fa-facebook"></i></a>
            <a class="facebook" target="_blank" href="mailto:admin@shopnotransport.com"><i class="fa fa-envelope"></i></a>
            <a class="facebook" target="_blank" href="https://play.google.com/store?hl=en"><i class="fa fa-android"></i></a>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="footer-nav">
                    <li class="active"><a href="#" class="scroll-to">
                            <?php
                            if(checkSession()){
                                echo "হোম";
                            } else {
                                echo "Home";
                            }
                            ?>
                        </a></li>
                    <li><a href="#aboutus" class="scroll-to">
                            <?php
                            if(checkSession()){
                                echo "পরিচিতি";
                            } else {
                                echo "About";
                            }
                            ?>
                        </a></li>
                    <li><a href="#services" class="scroll-to">
                            <?php
                            if(checkSession()){
                                echo "সার্ভিস";
                            } else {
                                echo "Services";
                            }
                            ?>
                        </a></li>
                    <!--                            <li><a href="#reviews" class="scroll-to">Reviews</a></li>-->
                    <li><a href="#locations" class="scroll-to">
                            <?php
                            if(checkSession()){
                                echo "ঠিকানা";
                            } else {
                                echo "Locations";
                            }
                            ?>
                        </a></li>
                    <li><a href="#contact" class="scroll-to">
                            <?php
                            if(checkSession()){
                                echo "যোগাযোগ";
                            } else {
                                echo "Contact";
                            }
                            ?>
                        </a></li>
                    <li><a href="#registration" class="scroll-to">
                            <?php
                            if(checkSession()){
                                echo "নিবন্ধন";
                            } else {
                                echo "Registration";
                            }
                            ?>

                        </a></li>
                </ul>
                <div class="clearfix"></div>
                <p class="copyright">Crafted with <i class="fa fa-heart"> </i> by <a class="macro-class" href="http://shobarjonnoweb.com/">Shobarjonnoweb.com</a></p>
            </div>
        </div>
    </div>
</footer>
<section id="vehicles" class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="title wow fadeInDown" data-wow-offset="200">Vehicle Models - <span class="subtitle">Our rental fleet at a glance</span></h2>
        </div>

        <!-- Vehicle nav start -->
        <div class="col-md-3 wow fadeInUp" data-wow-offset="100">
            <div id="vehicle-nav-container">
                <ul class="vehicle-nav">
                    <li class="active"><a href="#vehicle-1">VW Golf VII</a><span class="active">&nbsp;</span></li>
                    <li><a href="#vehicle-2">Audi A1 S-LINE</a><span class="active">&nbsp;</span></li>
                    <li><a href="#vehicle-3">Toyota Camry</a><span class="active">&nbsp;</span></li>
                    <li><a href="#vehicle-4">BMW 320 ModernLine</a><span class="active">&nbsp;</span></li>
                    <li><a href="#vehicle-5">Mercedes-Benz GLK</a><span class="active">&nbsp;</span></li>
                    <li><a href="#vehicle-6">VW Passat CC</a><span class="active">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <!-- Vehicle nav end -->

        <!-- Vehicle 1 data start -->
        <div class="vehicle-data" id="vehicle-1">
            <div class="col-md-6 wow fadeIn" data-wow-offset="100">
                <div class="vehicle-img">
                    <img class="img-responsive" src="img/vehicle1.jpg" alt="Vehicle">
                </div>
            </div>
            <div class="col-md-3 wow fadeInUp" data-wow-offset="200">
                <div class="vehicle-price">$ 37.40 <span class="info">rent per day</span></div>
                <table class="table vehicle-features">
                    <tr>
                        <td>Model</td>
                        <td>Limousine</td>
                    </tr>
                    <tr>
                        <td>Doors</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Luggage</td>
                        <td>2 Suitcases / 2 Bags</td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <td>Automatic</td>
                    </tr>
                    <tr>
                        <td>Air conditioning</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Minimum age</td>
                        <td>25 years</td>
                    </tr>
                </table>
                <a href="#teaser" class="reserve-button scroll-to"><span class="glyphicon glyphicon-calendar"></span> Reserve now</a>
            </div>
        </div>
        <!-- Vehicle 1 data end -->

        <!-- Vehicle 2 data start -->
        <div class="vehicle-data" id="vehicle-2">
            <div class="col-md-6" data-wow-offset="200">
                <div class="vehicle-img">
                    <img class="img-responsive" src="img/vehicle2.jpg" alt="Vehicle">
                </div>
            </div>
            <div class="col-md-3" data-wow-offset="200">
                <div class="vehicle-price">$ 70.40 <span class="info">rent per day</span></div>
                <table class="table vehicle-features">
                    <tr>
                        <td>Model</td>
                        <td>Limousine</td>
                    </tr>
                    <tr>
                        <td>Doors</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Luggage</td>
                        <td>2 Suitcases / 2 Bags</td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <td>Automatic</td>
                    </tr>
                    <tr>
                        <td>Air conditioning</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Minimum age</td>
                        <td>25 years</td>
                    </tr>
                </table>
                <a href="#teaser" class="reserve-button scroll-to"><span class="glyphicon glyphicon-calendar"></span> Reserve now</a>
            </div>
        </div>
        <!-- Vehicle 1 data end -->

        <!-- Vehicle 3 data start -->
        <div class="vehicle-data" id="vehicle-3">
            <div class="col-md-6" data-wow-offset="200">
                <div class="vehicle-img">
                    <img class="img-responsive" src="img/vehicle3.jpg" alt="Vehicle">
                </div>
            </div>
            <div class="col-md-3" data-wow-offset="200">
                <div class="vehicle-price">$ 100.40 <span class="info">rent per day</span></div>
                <table class="table vehicle-features">
                    <tr>
                        <td>Model</td>
                        <td>Limousine</td>
                    </tr>
                    <tr>
                        <td>Doors</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Luggage</td>
                        <td>2 Suitcases / 2 Bags</td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <td>Automatic</td>
                    </tr>
                    <tr>
                        <td>Air conditioning</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Minimum age</td>
                        <td>25 years</td>
                    </tr>
                </table>
                <a href="#teaser" class="reserve-button scroll-to"><span class="glyphicon glyphicon-calendar"></span> Reserve now</a>
            </div>
        </div>
        <!-- Vehicle 3 data end -->

        <!-- Vehicle 4 data start -->
        <div class="vehicle-data" id="vehicle-4">
            <div class="col-md-6" data-wow-offset="200">
                <div class="vehicle-img">
                    <img class="img-responsive" src="img/vehicle4.jpg" alt="Vehicle">
                </div>
            </div>
            <div class="col-md-3" data-wow-offset="200">
                <div class="vehicle-price">$ 100.40 <span class="info">rent per day</span></div>
                <table class="table vehicle-features">
                    <tr>
                        <td>Model</td>
                        <td>Limousine</td>
                    </tr>
                    <tr>
                        <td>Doors</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Luggage</td>
                        <td>2 Suitcases / 2 Bags</td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <td>Automatic</td>
                    </tr>
                    <tr>
                        <td>Air conditioning</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Minimum age</td>
                        <td>25 years</td>
                    </tr>
                </table>
                <a href="#teaser" class="reserve-button scroll-to"><span class="glyphicon glyphicon-calendar"></span> Reserve now</a>
            </div>
        </div>
        <!-- Vehicle 4 data end -->

        <!-- Vehicle 5 data start -->
        <div class="vehicle-data" id="vehicle-5">
            <div class="col-md-6" data-wow-offset="200">
                <div class="vehicle-img">
                    <img class="img-responsive" src="img/vehicle5.jpg" alt="Vehicle">
                </div>
            </div>
            <div class="col-md-3" data-wow-offset="200">
                <div class="vehicle-price">$ 100.40 <span class="info">rent per day</span></div>
                <table class="table vehicle-features">
                    <tr>
                        <td>Model</td>
                        <td>Limousine</td>
                    </tr>
                    <tr>
                        <td>Doors</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Luggage</td>
                        <td>2 Suitcases / 2 Bags</td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <td>Automatic</td>
                    </tr>
                    <tr>
                        <td>Air conditioning</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Minimum age</td>
                        <td>25 years</td>
                    </tr>
                </table>
                <a href="#teaser" class="reserve-button scroll-to"><span class="glyphicon glyphicon-calendar"></span> Reserve now</a>
            </div>
        </div>
        <!-- Vehicle 5 data end -->

        <!-- Vehicle 6 data start -->
        <div class="vehicle-data" id="vehicle-6">
            <div class="col-md-6" data-wow-offset="200">
                <div class="vehicle-img">
                    <img class="img-responsive" src="img/vehicle6.jpg" alt="Vehicle">
                </div>
            </div>
            <div class="col-md-3" data-wow-offset="200">
                <div class="vehicle-price">$ 100.40 <span class="info">rent per day</span></div>
                <table class="table vehicle-features">
                    <tr>
                        <td>Model</td>
                        <td>Limousine</td>
                    </tr>
                    <tr>
                        <td>Doors</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Luggage</td>
                        <td>2 Suitcases / 2 Bags</td>
                    </tr>
                    <tr>
                        <td>Transmission</td>
                        <td>Automatic</td>
                    </tr>
                    <tr>
                        <td>Air conditioning</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>Minimum age</td>
                        <td>25 years</td>
                    </tr>
                </table>
                <a href="#teaser" class="reserve-button scroll-to"><span class="glyphicon glyphicon-calendar"></span> Reserve now</a>
            </div>
        </div>
        <!-- Vehicle 6 data end -->

    </div>
</section>
<section id="aboutus" class="container wow fadeInUp about">
    <div class="row text-center">
        <div class="col-md-12 title">
            <h2>
                <?php
                if(checkSession()){
                    echo "পরিচিতি";
                } else {
                    echo "About Us";
                }
                ?>
            </h2>
            <span class="underline">&nbsp;</span>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div id="reviews-carousel" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">

                    <!-- Review item 1 start -->
                    <div class="item active">
                        <div class="topic">
                            <?php
                            if(checkSession()){
                                echo "স্বপ্ন ট্রান্সপোর্ট";
                            } else {
                                echo "Shopno Transport";
                            }
                            ?>
                        </div>
                        <div class="topic_description">
                            <?php
                            if(checkSession()){
                                echo "আপনার আরামদায়োক ভ্রমনের
                                        বিশ্বস্ত নাম।
                                         গ্রাহকদের নিরাপদে
                                        নির্দিষ্ট গন্তব্যে পৌছে
                                        দেয়ায় SHOPNO এর একমাত্র লক্ষ ও
                                        উদ্দেশ্য। এ জন্য SHOPNO এর টিম
                                        সর্বদা কাজ করে যাচ্ছে .
                                         ব্যবসা নয় সেবার মাধ্যমে
                                        স্বপ্নের পথো চলা।
                                         গ্রাহকদের সর্বোচ্চ
                                        নিরাপত্তার সাথে নিরাপদ এ
                                        ভ্রমন নিশ্চিত করতে স্বপ্ন
                                        সর্বোদা সচেষ্ঠ।";
                            } else {
                                echo "traveling to your comfort zone
                            Trusted name.
                            Customers can safely
                            Reaches specific destination
                            the onlyone target of SHOPNO is to give
                            Purpose. SHOPNO's team
                            always working because of
                            it is not our business,we work for the service.
                            SHOPNO is always ready for customer service.";
                            }
                            ?>
                        </div>
                    </div>
                    <!-- Review item 1 end -->

                    <div class="item">
                        <div class="topic">
                            <?php
                            if(checkSession()){
                                echo "স্বপ্ন কেন সেরা????";
                            } else {
                                echo "WHY SHOPNO IS THE BEST?";
                            }
                            ?>
                        </div>
                        <div class="topic_description">
                            <?php
                            if(checkSession()){
                                echo "#কারন স্বপ্নই দিচ্ছে
                                        সবচেয়ে কম খরচে ও অল্প সময়ে
                                        নির্দিষ্ট গন্তব্যে
                                        আরামদায়ক ভ্রমনের
                                        নিশ্চয়তা। #কারন স্বপ্নের আছে দেশ সেরা
                                        ড্রাইভার ও সর্ব প্রকার
                                        উন্নত গাড়ির বিশাল সমাহার। #শুধুমাত্র আপনার একটি
                                        মাত্র ফোন কলেই নির্দিষ্ট
                                        সময়ে আপনার পছন্দমত গাড়ি
                                        আপনার দোর গোড়ায় হাজির।শুধু
                                        মাত্র আমরা আমাদের
                                        রেজিস্ট্রার
                                         গ্রাহক দের সেবা প্রদান করে
                                        থাকি .";
                            } else {
                                echo "Because SHOPNO is giving
                            At the lowest cost and in a short time
                            At certain destination
                            Comfortable travel
                            Confirmation. Because SHOPNO is the best in the country
                            Driver and All Types
                            The huge combination of advanced cars.
                            Is your only one
                            phone calls are specific
                            Your favorite car at the time
                            Appear on your doorstep
                            Just we are ours
                            Registrar
                            Provides customer service
                            Be there";
                            }
                            ?>
                        </div>
                        <div class="topic_description">
                            <?php
                            if(checkSession()){
                                echo "গ্রাহকদের নির্ধারিত ফ্রী
                                        এর অতিরিক্ত কোন প্রকার
                                        বাড়তি বিল দিতে হয়না।
                                        অতিরিক্ত সম্পূর্ণ খরচ SHOPNO
                                        এর কর্তৃপক্ষের ।
                                         সবার নিরাপত্তা কামনায়
                                        স্বপ্ন টান্সর্পোট।";
                            } else {
                                echo "Free to the customers
                            No additional types of
                            Do not pay extra bills.
                            Additional full cost SHOPNO
                            Its authority. Encourage everyone's safety
                            SHOPNO  TRANSPORT.";
                            }
                            ?>

                        </div>
                    </div>

                </div>

                <!-- Review Nav start -->
                <ol class="carousel-indicators">
                    <li data-target="#reviews-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#reviews-carousel" data-slide-to="1"></li>
                </ol>
                <!-- Review Nav end -->
            </div>
        </div>
    </div>
</section>
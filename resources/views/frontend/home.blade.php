@extends('layouts.frontend')

@section('content')


        <!-- Header start -->
        @include('frontend.header')
        <!-- Header end -->
 <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('info'))
                                <div class="alert alert-info alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('info') !!}</li>
                                    </ul>
                                </div>
                            @endif
                        @if (Session::has('warning'))
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('warning') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('danger'))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('danger') !!}</li>
                                    </ul>
                                </div>
                            @endif
                    </div>
                </div>


        <!-- Teaser start -->
        @include('frontend.slider_teaser')
        <div class="arrow-down"></div>
        <!-- Teaser end -->



        <!--About start-->
        @include('frontend.about')
        <!--About End-->

        <!-- Services start -->
        @include('frontend.services')
        <!-- Services end -->

        <!-- Newsletter start -->
        @include('frontend.subscribers')
        <!-- Newsletter end -->




        <!-- Reviews start -->
        @include('frontend.review')
        <!-- Reviews end -->


        <!-- Locations start -->
        @include('frontend.location')
        <!-- Locations end -->



        <!-- Information start -->
        <section id="information" class="container">

          <!-- Single photo start -->
          <div class="row wow fadeInLeft" data-wow-offset="100">
            <div class="col-md-6 col-xs-12 pull-right">
              <img src="{{ asset('public/frontend/img/quality.jpg') }}" alt="Info Img" class="img-responsive">
            </div>
            <div class="col-md-6 pull-left">
              <h2 class="title">
                  <?php
                  if(checkSession()){
                      echo "নিশ্চিত গুনগত মান";
                  } else {
                      echo "Quality Guaranteed";
                  }
                  ?>
              </h2>
                <?php
                if(checkSession()){
                    echo '<h3 class="subtitle">গুনগত মান নিশ্চিত করতে আমরা, স্বপ্ন ট্রান্সপোর্ট টিম সর্বদা সজাগ থাকি।</h3>
                <p>গ্রাহকবৃন্দ আমাদের নিকট যে কোয়ালিটির সার্ভিস চান, আমরা সে মানের সার্ভিসে দিতে সর্বোদা ওয়াদাবদ্ধ।</p>';
                } else {
                    echo '<h3 class="subtitle">About quality maintenance, we the Shopno Transport team, always stay sharp.</h3>
                <p>We are vowed to deliver our best to fulfill our client\'s choice.</p>';
                }
                ?>
            </div>
          </div>
          <!-- Single photo end -->
        </section>
        <!-- Information end -->



        @include('frontend.partners')


        <!-- Contact start -->
        @include('frontend.contact')
        <!-- Contact end -->


<!--        Registration-->
        @include('frontend.registration')
<!--        Registration-->


        <a href="#" class="scrollup">ScrollUp</a>


        <!-- Footer start -->
        @include('frontend.footer')
        <!-- Footer end -->



        <!-- Checkout Modal Start -->
        @include('frontend.checkout_modal')
        <!-- Checkout Modal end -->



@endsection
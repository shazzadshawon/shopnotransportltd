<section id="registration" class="container wow bounceInUp" data-wow-offset="50">
    <div class="row">
        <div class="col-md-12">
            <?php
            if(checkSession()){
                echo '<h2>নিবন্ধন</h2>';
            } else {
                echo '<h2>Registration</h2>';
            }
            ?>
        </div>

        <div class="col-md-12 col-xs-12">
            <form action="{{ url('storeregistration') }}" method="post" id="contact-form" name="contact-form"  enctype="multipart/form-data">
{{ csrf_field() }}   
                <div class="alert hidden" id="contact-form-msg"></div>

                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="text" class="form-control name text-field" name="name" placeholder="নাম:">
                            <div class="clearfix"></div>
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="text" class="form-control name text-field" name="name" placeholder="Name:">
                            <div class="clearfix"></div>
                        </div>';
                }
                ?>


                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="number" class="form-control nid text-field" name="nid" placeholder="এন আই ডি:">
                            <div class="clearfix"></div>
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="number" class="form-control nid text-field" name="nid" placeholder="NID:">
                            <div class="clearfix"></div>
                        </div>';
                }
                ?>


                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="text" class="form-control fb text-field" name="fb" placeholder="ফেসবুক লিংক:">
                            <div class="clearfix"></div>
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="text" class="form-control fb text-field" name="fb" placeholder="FB Link:">
                            <div class="clearfix"></div>
                        </div>';
                }
                ?>

                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="tel" class="form-control telephone text-field" name="telephone" placeholder="ফোন:">
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="tel" class="form-control telephone text-field" name="telephone" placeholder="Telephone:">
                        </div>';
                }
                ?>


                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="email" class="form-control email text-field" name="email" placeholder="ইমেইল:">
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="email" class="form-control email text-field" name="email" placeholder="Email:">
                        </div>';
                }
                ?>


                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <textarea class="form-control address" name="address" placeholder="ঠিকানা:"></textarea>
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <textarea class="form-control address" name="address" placeholder="Address:"></textarea>
                        </div>';
                }
                ?>


                <div class="form-group">
                    <input type="file" class="form-control file text-field" name="photo">
                </div>


                <?php
                if(checkSession()){
                    echo '<input type="submit" class="btn submit-message" name="submit-message" value="নিবন্ধন করুন">';
                } else {
                    echo '<input type="submit" class="btn submit-message" name="submit-message" value="Register">';
                }
                ?>


            </form>
        </div>

    </div>
</section>
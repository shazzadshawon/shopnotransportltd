<section id="contact" class="container wow bounceInUp" data-wow-offset="50">
    <div class="row">
        <div class="col-md-12">
            <h2>
                <?php
                if(checkSession()){
                    echo "আমাদের সাথে যোগাযোগ করুন";
                } else {
                    echo "Contact Us";
                }
                ?>
            </h2>
        </div>
        <div class="col-md-4 col-xs-12 pull-right">
            <h4 class="contact-box-title">
                <?php
                if(checkSession()){
                    echo "কাস্টমার সেন্টার";
                } else {
                    echo "Customer Center";
                }
                ?>
            </h4>
            @foreach ($cares as $care)
                <div class="contact-box">
                <img src="{{ asset('public/uploads/customer/'.$care->image) }}" style="width: 80px; height: 80px" alt="contact-img">
                <div class="contact-box-name">{{ $care->name }}</div>
                <div class="contact-box-phon"><span class="highlight">Phone:</span> {{ $care->mobile }}</div>
                <div class="contact-box-email"><span class="highlight">Email:</span> {{ $care->email }}</div>
                <div class="clearfix"></div>
            </div>
            <div class="contact-box-border">&nbsp;</div>
            @endforeach
           

            <div class="contact-box-divider">&nbsp;</div>

            <h4 class="contact-box-title">
                <?php
                if(checkSession()){
                    echo "রিজার্ভেশন পরিবর্তন বা বাতিলকরন";
                } else {
                    echo "Change or Cancel Reservation";
                }
                ?>
            </h4>
              </h4>
            @foreach ($cancels as $care)
                <div class="contact-box">
                <img src="{{ asset('public/uploads/customer/'.$care->image) }}" style="width: 80px; height: 80px" alt="contact-img">
                <div class="contact-box-name">{{ $care->name }}</div>
                <div class="contact-box-phon"><span class="highlight">Phone:</span> {{ $care->mobile }}</div>
                <div class="contact-box-email"><span class="highlight">Email:</span> {{ $care->email }}</div>
                <div class="clearfix"></div>
            </div>
            <div class="contact-box-border">&nbsp;</div>
            @endforeach

        </div>
        <div class="col-md-8 col-xs-12 pull-left">
            <p class="contact-info">
                <?php
                if(checkSession()){
                    echo "যে কোন প্রয়োজনে আমাদেরকে পাবেন - ০১৭৪৪৭৭৪০৩৪";
                } else {
                    echo "For any kind of deal, please feel free to contact us - 01744774034";
                }
                ?>
                <br>

                <?php
                if(checkSession()){
                    echo '<span class="address"><span class="highlight">ঠিকানা:</span> Shopno|Transport হেড অফিস - রোড - ১১, বনানি, ঢাকা-১২১৩</span>';
                } else {
                    echo '<span class="address"><span class="highlight">Address:</span> Shopno|Transport Head Office - Road - 11, Banani, Dhaka-1213</span>';
                }
                ?>
                </p>
            <form action="{{ url('storecontact') }}" method="post" id="contact-form" name="contact-form">
                {{ csrf_field() }}   
                <div class="alert hidden" id="contact-form-msg"></div>

                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="text" class="form-control first-name text-field" name="first-name" placeholder="ফার্স্ট নাম:">
                            <input type="text" class="form-control last-name text-field" name="last-name" placeholder="লাস্ট নাম:">
                            <div class="clearfix"></div>
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="text" class="form-control first-name text-field" name="first-name" placeholder="First Name:">
                            <input type="text" class="form-control last-name text-field" name="last-name" placeholder="Last Name:">
                            <div class="clearfix"></div>
                        </div>';
                }
                ?>

                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="tel" class="form-control telephone text-field" name="telephone" placeholder="টেলিফোন:">
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="tel" class="form-control telephone text-field" name="telephone" placeholder="Telephone:">
                        </div>';
                }
                ?>

                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <input type="email" class="form-control email text-field" name="email" placeholder="ইমেইল:">
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <input type="email" class="form-control email text-field" name="email" placeholder="Email:">
                        </div>';
                }
                ?>


                <?php
                if(checkSession()){
                    echo '<div class="form-group">
                            <textarea class="form-control message" name="message" placeholder="মেসেজ:"></textarea>
                        </div>';
                } else {
                    echo '<div class="form-group">
                            <textarea class="form-control message" name="message" placeholder="Message:"></textarea>
                        </div>';
                }
                ?>

                <?php
                if(checkSession()){
                    echo '<input type="submit" class="btn submit-message" name="submit-message" value="পাঠান">';
                } else {
                    echo '<input type="submit" class="btn submit-message" name="submit-message" value="Submit Message">';
                }
                ?>


            </form>
        </div>

    </div>
</section>
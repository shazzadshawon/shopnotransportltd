<?php
       if(!session_id()) {
             session_start();
          }

    if (isset($_GET['lang'])){
        $_SESSION['lang'] = $_GET['lang'];
    }

    function checkSession(){
        if(isset($_SESSION['lang'])){
            if($_SESSION['lang'] == 'bn'){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
?>


<header data-spy="affix" data-offset-top="39" data-offset-bottom="0" class="large">

    <div class="row container box">
        <div class="col-md-5">
            <!-- Logo start -->
            <div class="brand">
                <h1><a href="#top" class="scroll-to"><img  src="{{url('public/frontend/img/logo_shopno.png')}}" class="img-responsive" alt="Shopno|Transport"></a></h1>
            </div>
            <!-- Logo end -->
        </div>

        <div class="col-md-7">
            <div class="pull-right">
                <div class="header-info pull-right">
                    <div class="contact pull-left">
                        <?php
                       if(checkSession()){
                            echo "যোগাযোগঃ +৮৮০ ১৭৪৪৭ ৭৪০৩৪";
                        } else {
                            echo "CONTACT: +880 17447 74034";
                        }
                        ?>
                    </div>
                    <!-- Language Switch start -->
                    <div class="language-switch pull-right">
                        <div class="dropdown">
                            <a data-toggle="dropdown" href="#" id="language-switch-dropdown">
                                <?php
                                if(checkSession()){
                                    echo "ভাষা নির্বাচন করুন";
                                } else {
                                    echo "Select your language";
                                }
                                ?>

                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="language-switch-dropdown">
                                <li><a href="#" onclick="setGetData('lang', 'en')"><img src="{{ url('public/frontend/img/flags/United-States.png') }}" alt="usa"> English</a></li>
                                <li><a href="#" onclick="setGetData('lang', 'bn')"><img src="{{ url('public/frontend/img/flags/Bangladesh.png') }}" alt="bangali"> Bangali</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Language Switch end -->
                </div>
            </div>

            <div class="clearfix"></div>

            <!-- start navigation -->
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand scroll-to" href="#top"><img class="img-responsive"  src="{{url('public/frontend/img/logo_shopno_m.png')}}" alt="Shopno|Transport"></a>
                    </div>
                    <!-- Collect the nav links, for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <!-- Nav-Links start -->
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#" class="scroll-to">
                                    <?php
                                    if(checkSession()){
                                        echo "হোম";
                                    } else {
                                        echo "Home";
                                    }
                                    ?>
                                </a></li>
                            <li><a href="#aboutus" class="scroll-to">
                                    <?php
                                    if(checkSession()){
                                        echo "পরিচিতি";
                                    } else {
                                        echo "About";
                                    }
                                    ?>
                                </a></li>
                            <li><a href="#services" class="scroll-to">
                                    <?php
                                    if(checkSession()){
                                        echo "সার্ভিস";
                                    } else {
                                        echo "Services";
                                    }
                                    ?>
                                </a></li>
<!--                            <li><a href="#reviews" class="scroll-to">Reviews</a></li>-->
                            <li><a href="#locations" class="scroll-to">
                                    <?php
                                    if(checkSession()){
                                        echo "ঠিকানা";
                                    } else {
                                        echo "Locations";
                                    }
                                    ?>
                                </a></li>
                            <li><a href="#contact" class="scroll-to">
                                    <?php
                                    if(checkSession()){
                                        echo "যোগাযোগ";
                                    } else {
                                        echo "Contact";
                                    }
                                    ?>
                                </a></li>
                            <li><a href="#registration" class="scroll-to">
                                    <?php
                                    if(checkSession()){
                                        echo "নিবন্ধন";
                                    } else {
                                        echo "Registration";
                                    }
                                    ?>

                                </a></li>
                        </ul>
                        <!-- Nav-Links end -->
                    </div>
                </div>
            </nav>
            <!-- end navigation -->
        </div>
    </div>

</header>

<script type="text/javascript">
    
function setGetData(key, value) {
    key = encodeURI(key); value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');

    var i=kvp.length; var x; while(i--)
    {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&');
}

</script>
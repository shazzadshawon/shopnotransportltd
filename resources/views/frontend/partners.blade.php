<section id="partners" class="wow fadeIn" data-wow-offset="50">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>
                    <?php
                    if(checkSession()){
                        echo "<h2>আমাদের সেবা নিয়েছেন যারা</h2>";
                    } else {
                        echo "<h2>Meet our Partners</h2>";
                    }
                    ?>
                </h2>
                <span class="underline">&nbsp;</span>
                <?php
                if(checkSession()){
                    echo "<p>ইতিবাচক পরিবর্তন অবদান এবং আমাদের টেকসই লক্ষ্য অর্জন, আমরা বিশ্বের অনেক অসাধারণ প্রতিষ্ঠানের সাথে অংশীদার তাদের দক্ষতা আমরা একা আমরা একা বেশী করতে আমাদের সক্ষম করে, এবং তাদের আবেগ এবং প্রতিভা আমাদের অনুপ্রেরণা। এটি এমন একটি আনন্দদায়ক প্রতিষ্ঠানের সাথে পরিচয় করানোর জন্য আমাদের পরিতৃপ্তি, যার সাফল্য এবং প্রতিশ্রুতিগুলি আমরা আমাদের অংশীদারদের কল্যাণে ভাগ্যবান সব প্রতিষ্ঠানের প্রতিনিধি।</p>";
                } else {
                    echo "<p>To contribute to positive change and achieve our sustainability goals, we partner with many extraordinary organizations around the world. Their expertise enables us to do far more than we could alone, and their passion and talent inspire us. It is our pleasure to introduce you to a handful of the organizations whose accomplishments and commitments are representative of all the organizations we are fortunate to call our partners.</p>";
                }
                ?>
            </div>
            <!--@foreach ($partners as $partner)-->
            <!--     <div class="col-md-3 col-xs-6 text-center clearfix">-->
            <!--        <img src="{{ asset('public/uploads/partner/'.$partner->logo) }}" alt="Partner" class="img-responsive wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="200">-->
            <!--    </div>-->
            <!--    <br>-->
            <!--@endforeach-->
           
          
        </div>
         
            @for($i=0;$i<count($partners); $i+=4)
                <div class="row partner_margin">
                    @if(!empty($partners[$i]))
                     <div class="col-md-3 col-xs-6 text-center clearfix">
                        <img src="{{ asset('public/uploads/partner/'.$partners[$i]->logo) }}" alt="Partner" class="img-responsive wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="200">
                    </div>
                    @endif
                    
                     @if(!empty($partners[$i+1]))
                     <div class="col-md-3 col-xs-6 text-center clearfix">
                        <img src="{{ asset('public/uploads/partner/'.$partners[$i+1]->logo) }}" alt="Partner" class="img-responsive wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="200">
                    </div>
                    @endif
                    
                     @if(!empty($partners[$i+2]))
                    
                     <div class="col-md-3 col-xs-6 text-center clearfix">
                        <img src="{{ asset('public/uploads/partner/'.$partners[$i+2]->logo) }}" alt="Partner" class="img-responsive wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="200">
                    </div>
                    @endif
                    
                     @if(!empty($partners[$i+3]))
                    <div class="col-md-3 col-xs-6 text-center clearfix">
                        <img src="{{ asset('public/uploads/partner/'.$partners[$i+3]->logo) }}" alt="Partner" class="img-responsive wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="200">
                    </div>
                    @endif
                   
                    
                     
                </div>
            @endfor
    </div>
</section>
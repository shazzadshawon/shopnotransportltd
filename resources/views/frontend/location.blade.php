<section id="locations">
    <div class="container location-select-container wow bounceInDown" data-wow-offset="200">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="location-select">
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            if(checkSession()){
                                echo "<h2>স্বপ্ন ট্রান্সপোর্ট</h2>";
                            } else {
                                echo "<h2>Shopno Transport</h2>";
                            }
                            ?>
                        </div>
                        <div class="col-md-6">
                            <div class="styled-select-location">
                                <select id="location-map-select"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="arrow-down-location">&nbsp;</div>
    </div>
    <div class="map wow bounceInUp" data-wow-offset="100"><!-- map by gmap3 --></div>
</section>

<!--<script src="{{ asset('public/frontend/js/locations-autocomplete.js') }}"></script>-->
<!-- <script src="{{ asset('public/frontend/js/custom.js') }}"></script>-->
<section id="reviews" class="container wow fadeInUp">
    <div class="row text-center">
        <div class="col-md-12 stars">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star big"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div id="reviews-carousel" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">

                  @php
                    $i=0;
                @endphp

                @foreach ($reviews as $review)
                   <div class="item @if($i==0) active @endif">
                        <div class="review">
                            <?php
                            if(checkSession()){
                                echo $review->review_description_bn;
                            } else {
                                echo $review->review_description;
                            }
                            ?>
                        </div>
                        <div class="author">
                            <?php
                            if(checkSession()){
                                echo "- ".$review->review_title_bn;
                            } else {
                                echo "- ".$review->review_title;
                            }
                            ?>
                        </div>
                    </div>

                  @php
                    $i++;
                @endphp
                @endforeach



                </div>

                <!-- Review Nav start -->
                @php
                    $j=0;
                @endphp
                <ol class="carousel-indicators">
                @foreach ($reviews as $review)
                      <li data-target="#reviews-carousel" data-slide-to="{{ $j }}" class="@if($j==0) active @endif"></li>
                      @php
                          $j++;
                      @endphp
                @endforeach
                  
                </ol>
                <!-- Review Nav end -->

            </div>
        </div>
    </div>
</section>
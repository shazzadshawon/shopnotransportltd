<section id="services" class="container">
    <div class="row">
        <div class="col-md-12 title">
            <h2>
                <?php
                if(checkSession()){
                    echo "কাস্টমার সার্ভিস / প্যাকেজেস";
                } else {
                    echo "Customer services / Packages";
                }
                ?>
            </h2>
            <span class="underline">&nbsp;</span>
        </div>

        <!-- Service Box start -->
        <div class="col-md-6">
            <div class="service-box wow fadeInLeft" data-wow-offset="100">
                <div class="service-icon">+</div>
                <h3 class="service-title">
                    <?php
                    if(checkSession()){
                        echo "গাড়ি বুকিং এর বিশেষ রেট";
                    } else {
                        echo "Special rates on car booking";
                    }
                    ?>
                </h3>
                <div class="clearfix"></div>
                <div class="container-fluid">

                @foreach ($cars as $car)
                
                    @php
                        $services = DB::table('item2')->where('car_id',$car->id)->get();
                    @endphp
                    
                    @if(count($services) > 0)
                    <h3>
                        @if(checkSession())
                        {{ $car->service_title_bn }}
                        @else
                        {{ $car->service_title }}
                        @endif
                    </h3>
                    @endif

                   

                     <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                            @foreach ($services as $service)
                            

                            <?php
                            if(checkSession()){
                                echo "<tr>
                                        <td>".$service->place_bn."</td>
                                        <td>".$service->duration_bn."</td>
                                        <td>".$service->cost_bn."</td>
                                    </tr>";
                            } else {
                                echo "<tr>
                                        <td>".$service->place."</td>
                                        <td>".$service->duration."</td>
                                        <td>".$service->cost."</td>
                                    </tr>";
                            }
                            ?>

                            @endforeach
                           

                            </tbody>
                        </table>
                    </div>

                @endforeach

                    


                </div>
            </div>
        </div>
        <!-- Service Box end -->

        <!-- Service Box start -->
        <div class="col-md-6">
            <div class="service-box wow fadeInRight" data-wow-offset="100">
                <div class="service-icon">+</div>
                <h3 class="service-title">
                    <?php
                    if(checkSession()){
                        echo "মোবাইল ফোন রিজার্ভেশান ";
                    } else {
                        echo "Mobile Phone Reservation";
                    }
                    ?>
                </h3>
                <div class="clearfix"></div>
                <p class="service-content">
                    <?php
                    if(checkSession()){
                        echo "আমরা আরো গ্রাহক প্রতিক্রিয়া জন্য মোবাইল ফোন সংরক্ষণ সেবা প্রদান করে থাকি।";
                    } else {
                        echo "We provide Mobile Phone Reservation service for more customer response.";
                    }
                    ?>
                </p>
                <form action="{{ url('email_reserve') }}" method="post" name="mobile_email_reservation-form" id="mobile_email_reservation-form">
                {{ csrf_field() }}
                    <div class="input-group">
                        

                    <input type="email" name="email" class="form-control" placeholder="@if(checkSession())আপনার ইমেইল এড্রেস দিন @else Enter your Email Address @endif"> 
                    <span class="input-group-btn">
                        <input class="btn btn-default button" type="submit" value="@if(checkSession())পাঠান @else Send @endif"> 
                    </span>
                        
                   
                    </div>
                </form>

                <form action="{{ url('mobile_reserve') }}" method="post" name="mobile_phone_reservation-form" id="mobile_phone_reservation-form">
                {{ csrf_field() }}

                    <div class="input-group">
                        
                        {{-- //if(checkSession()){ --}}
                    <input type="text" name="phone" class="form-control" placeholder="@if(checkSession())আপনার ফোন নাম্বার দিন। @else Enter your Phone Number @endif"> 
                    <span class="input-group-btn">
                         <input class="btn btn-default button" type="submit" value="@if(checkSession())পাঠান @else Send @endif"> 
                    </span>
                        
                   
                    </div>


                </form>
            </div>
        </div>
        <!-- Service Box end -->

        <!-- Service Box start -->
        <div class="col-md-6">
            <div class="service-box wow fadeInLeft" data-wow-offset="100">
                <div class="service-icon">+</div>
                <h3 class="service-title">
                    <?php
                    if(checkSession()){
                        echo "যত ইচ্ছা তত মাইলেজ";
                    } else {
                        echo "Unlimited Miles Car Rental";
                    }
                    ?>
                </h3>
                <div class="clearfix"></div>
                <p class="service-content">
                    <?php
                    if(checkSession()){
                        echo "মগ্রাহকদের জন্য শুধুমাত্র আমরা আনলিমিটেড মাইলস গাড়ি ভাড়া সেবা প্রদান করি। তার কারন আমাদের ক্লায়েন্ট সময় কোন মুহূর্তে কোন দূরত্ব ভ্রমণ করতে পারেন। শুধুমাত্র একটি কল বা একটি এসএমএস পরিষেবা শুরু ";
                    } else {
                        echo "Only we provide Unlimited Miles Car Rental service for the customers. Thats why our clients can travel to any distance at any moment of time. Only a call or a SMS initiate the service.";
                    }
                    ?>
                </p>
            </div>
        </div>
        <!-- Service Box end -->

        <!-- Service Box start -->
        <div class="col-md-6">
            <div class="service-box wow fadeInRight" data-wow-offset="100">
                <div class="service-icon">+</div>
                <h3 class="service-title">
                    <?php
                    if(checkSession()){
                        echo "শীঘ্রই আসছে";
                    } else {
                        echo "Comming Soon";
                    }
                    ?>
                </h3>
                <div class="clearfix"></div>
                <p class="service-content">
                    <?php
                    if(checkSession()){
                        echo "শীঘ্রই আসছে নতুন আকর্ষণীয় কিছু সার্ভিস";
                    } else {
                        echo "New attractive services are coming soon for our customers.";
                    }
                    ?>
                </p>
            </div>
        </div>
        <!-- Service Box end -->

    </div>
</section>
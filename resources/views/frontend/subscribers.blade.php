<section id="newsletter" class="wow slideInLeft" data-wow-offset="300">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><div class="alert hidden" id="newsletter-form-msg"></div></div>
            <div class="col-md-5 col-xs-12">
                <h2 class="title">
                    <?php
                    if(checkSession()){
                        echo "অভাবনীয় অফারের জন্য সাইন-আপ করুন";
                    } else {
                        echo "Sign up for amazing offers";
                    }
                    ?>
                    <span class="subtitle">
                        <?php
                        if(checkSession()){
                            echo "প্রস্তাব এবং প্রচারের জন্য একচেটিয়া অ্যাক্সেস";
                        } else {
                            echo "exclusive access for offers and promotions";
                        }
                        ?>
                    </span></h2>
            </div>
            <div class="col-md-7">
                <div class="newsletter-form pull-left">
                    <form action="{{ url('subscribe') }}" method="post" name="newsletter-form" id="newsletter-form">
                    {{ csrf_field() }}
                        <div class="input-group">
                            <?php
                            if(checkSession()){
                                echo '<input type="email" name="newsletter-email" class="form-control" placeholder="আপনার ইমেইল এড্রেস দিন">
                            <span class="input-group-btn">
                            <input class="btn btn-default button" type="submit" value="পাঠান">';
                            } else {
                                echo '<input type="email" name="newsletter-email" class="form-control" placeholder="Enter your Email Address">
                            <span class="input-group-btn">
                            <input class="btn btn-default button" type="submit" value="send">';
                            }
                            ?>
                        </span>
                        </div>
                    </form>
                </div>
                <div class="social-icons pull-right">
                    <ul>
                        <li><a class="facebook" href="https://www.facebook.com/kotkoti.transport"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="googleplus" href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
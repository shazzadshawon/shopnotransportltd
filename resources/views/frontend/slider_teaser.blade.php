<section id="teaser">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-12 pull-right">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides start -->
                    <div class="carousel-inner">
                    @php
                        $i=0;
                    @endphp
                    @foreach ($sliders as $slider)
                         <div class="item @if($i==0) active @endif">
                            <h1 class="title">
                                <?php
                                if(checkSession()){
                                    echo $slider->slider_title_bn;
                                } else {
                                    echo $slider->slider_title;
                                }
                                ?>
                                <span class="subtitle">
                                    <?php
                                    if(checkSession()){
                                        echo $slider->slider_subtitle_bn;
                                    } else {
                                        echo $slider->slider_subtitle;
                                    }
                                    ?>
                                </span>
                            </h1>
                            <div class="car-img">
                                <img src="{{ asset('public/uploads/slider/'.$slider->slider_image) }}" class="img-responsive" alt="hiace">
                            </div>
                        </div>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                       
                     
                    </div>
                    <!-- Wrapper for slides end -->

                    <!-- Slider Controls start -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    <!-- Slider Controls end -->
                </div>
            </div>
            <div class="col-md-5 col-xs-12 pull-left">
                <div class="reservation-form-shadow">

                    <form action="#" method="post" name="car-select-form" id="car-select-form">

                        <div class="alert alert-danger hidden" id="car-select-form-msg">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php
                            if(checkSession()){
                                echo "<strong>নোট:</strong> প্রতিটি ফিল্ড পূরণ করতে হবে!";
                            } else {
                                echo "<strong>Note:</strong> All fields required!";
                            }
                            ?>
                        </div>

                        <!-- Car select start -->
                        <div class="styled-select-car">
                            <select name="car-select" id="car-select">
                                <option value="">
                                    <?php
                                    if(checkSession()){
                                        echo "আপনার গাড়িটি নির্বাচন করুন";
                                    } else {
                                        echo "Select Your Car type";
                                    }
                                    ?>
                                </option>
                                @foreach ($cars as $car)
                                    @if(checkSession())
                                    <option data="{{ $car->id }}" value="{{ asset('public/uploads/item1/'.$car->image_name) }}">{{ $car->service_title_bn }}</option>
                                    @else
                                    <option data="{{ $car->id }}" value="{{ asset('public/uploads/item1/'.$car->image_name) }}">{{ $car->service_title }}</option>
                                    @endif
                                @endforeach
                                
                               
                            </select>
                        </div>
                        <!-- Car select end -->


                        <!-- Pick-up location start -->
                        <div class="location">
                            <div class="input-group pick-up">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span>
                                    <?php
                                    if(checkSession()){
                                        echo "পিক আপ";
                                    } else { 
                                        echo "Pick Up";
                                    }
                                    ?>
                                </span>
                                <?php
                                if(checkSession()){
                                    echo '<input type="text" name="pick-up-location" id="pick-up-location" class="form-control autocomplete-location" placeholder="আপনার শহর বা বিমানবন্দর">';
                                } else {
                                    echo '<input type="text" name="pick-up-location" id="pick-up-location" class="form-control autocomplete-location" placeholder="Enter a City or Airport">';
                                }
                                ?>
                            </div>
                            <!-- Pick-up location end -->

                            <a class="different-drop-off" href="#">
                                <?php
                                if(checkSession()){
                                    echo "ভিন্ন কোন ড্রপ অফ লোকেশান দেখাতে চান?";
                                } else {
                                    echo "Need a different drop-off location?";
                                }
                                ?>
                            </a>

                            <!-- Drop-off location start -->
                            <div class="input-group drop-off">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span>
                                    <?php
                                    if(checkSession()){
                                        echo "ড্রপ-অফ";
                                    } else {
                                        echo "Drop Off";
                                    }
                                    ?>
                                </span>
                                <?php
                                if(checkSession()){
                                    echo '<input type="text" name="drop-off-location" id="drop-off-location" class="form-control autocomplete-location" placeholder="আপনার শহর বা বিমানবন্দর">';
                                } else {
                                    echo '<input type="text" name="drop-off-location" id="drop-off-location" class="form-control autocomplete-location" placeholder="Enter a City or Airport">';
                                }
                                ?>
                            </div>
                        </div>
                        <!-- Drop-off location end -->

                        <!-- Pick-up date/time start -->
                        <div class="datetime pick-up">
                            <div class="date pull-left">
                                <div class="input-group">
                                    <span class="input-group-addon pixelfix"><span class="glyphicon glyphicon-calendar"></span>
                                        <?php
                                        if(checkSession()){
                                            echo "পিক-আপ  ";
                                        } else {
                                            echo "Pick-up";
                                        }
                                        ?>
                                    </span>
                                    <input type="text" name="pick-up-date" id="pick-up-date" class="form-control datepicker" placeholder="mm/dd/yyyy">
                                </div>
                            </div>
                            <div class="time pull-right">
                                <div class="styled-select-time">
                                    <select name="pick-up-time" id="pick-up-time">
                                        <option value="12:00 AM">12:00 AM</option>
                                        <option value="12:30 AM">12:30 AM</option>
                                        <option value="01:00 AM">01:00 AM</option>
                                        <option value="01:30 AM">01:30 AM</option>
                                        <option value="02:00 AM">02:00 AM</option>
                                        <option value="02:30 AM">02:30 AM</option>
                                        <option value="03:00 AM">03:00 AM</option>
                                        <option value="03:30 AM">03:30 AM</option>
                                        <option value="04:00 AM">04:00 AM</option>
                                        <option value="04:30 AM">04:30 AM</option>
                                        <option value="05:00 AM">05:00 AM</option>
                                        <option value="05:30 AM">05:30 AM</option>
                                        <option value="06:00 AM">06:00 AM</option>
                                        <option value="06:30 AM">06:30 AM</option>
                                        <option value="07:00 AM">07:00 AM</option>
                                        <option value="07:30 AM">07:30 AM</option>
                                        <option value="08:00 AM">08:00 AM</option>
                                        <option value="08:30 AM">08:30 AM</option>
                                        <option value="09:00 AM">09:00 AM</option>
                                        <option value="09:30 AM">09:30 AM</option>
                                        <option value="10:00 AM">10:00 AM</option>
                                        <option value="10:30 AM">10:30 AM</option>
                                        <option value="11:00 AM">11:00 AM</option>
                                        <option value="11:30 AM">11:30 AM</option>
                                        <option value="12:00 PM">12:00 PM</option>
                                        <option value="12:30 PM">12:30 PM</option>
                                        <option value="01:00 PM">01:00 PM</option>
                                        <option value="01:30 PM">01:30 PM</option>
                                        <option value="02:00 PM">02:00 PM</option>
                                        <option value="02:30 PM">02:30 PM</option>
                                        <option value="03:00 PM">03:00 PM</option>
                                        <option value="03:30 PM">03:30 PM</option>
                                        <option value="04:00 PM">04:00 PM</option>
                                        <option value="04:30 PM">04:30 PM</option>
                                        <option value="05:00 PM">05:00 PM</option>
                                        <option value="05:30 PM">05:30 PM</option>
                                        <option value="06:00 PM">06:00 PM</option>
                                        <option value="06:30 PM">06:30 PM</option>
                                        <option value="07:00 PM">07:00 PM</option>
                                        <option value="07:30 PM">07:30 PM</option>
                                        <option value="08:00 PM">08:00 PM</option>
                                        <option value="08:30 PM">08:30 PM</option>
                                        <option value="09:00 PM">09:00 PM</option>
                                        <option value="09:30 PM">09:30 PM</option>
                                        <option value="10:00 PM">10:00 PM</option>
                                        <option value="10:30 PM">10:30 PM</option>
                                        <option value="11:00 PM">11:00 PM</option>
                                        <option value="11:30 PM">11:30 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Pick-up date/time end -->

                        <!-- Drop-off date/time start -->
                        <div class="datetime drop-off">
                            <div class="date pull-left">
                                <div class="input-group">
                                    <span class="input-group-addon pixelfix"><span class="glyphicon glyphicon-calendar"></span>
                                        <?php
                                        if(checkSession()){
                                            echo "ড্রপ-অফ";
                                        } else {
                                            echo "Drop Off";
                                        }
                                        ?>
                                    </span>
                                    <input type="text" name="drop-off-date" id="drop-off-date" class="form-control datepicker" placeholder="mm/dd/yyyy">
                                </div>
                            </div>
                            <div class="time pull-right">
                                <div class="styled-select-time">
                                    <select name="drop-off-time" id="drop-off-time">
                                        <option value="12:00 AM">12:00 AM</option>
                                        <option value="12:30 AM">12:30 AM</option>
                                        <option value="01:00 AM">01:00 AM</option>
                                        <option value="01:30 AM">01:30 AM</option>
                                        <option value="02:00 AM">02:00 AM</option>
                                        <option value="02:30 AM">02:30 AM</option>
                                        <option value="03:00 AM">03:00 AM</option>
                                        <option value="03:30 AM">03:30 AM</option>
                                        <option value="04:00 AM">04:00 AM</option>
                                        <option value="04:30 AM">04:30 AM</option>
                                        <option value="05:00 AM">05:00 AM</option>
                                        <option value="05:30 AM">05:30 AM</option>
                                        <option value="06:00 AM">06:00 AM</option>
                                        <option value="06:30 AM">06:30 AM</option>
                                        <option value="07:00 AM">07:00 AM</option>
                                        <option value="07:30 AM">07:30 AM</option>
                                        <option value="08:00 AM">08:00 AM</option>
                                        <option value="08:30 AM">08:30 AM</option>
                                        <option value="09:00 AM">09:00 AM</option>
                                        <option value="09:30 AM">09:30 AM</option>
                                        <option value="10:00 AM">10:00 AM</option>
                                        <option value="10:30 AM">10:30 AM</option>
                                        <option value="11:00 AM">11:00 AM</option>
                                        <option value="11:30 AM">11:30 AM</option>
                                        <option value="12:00 PM">12:00 PM</option>
                                        <option value="12:30 PM">12:30 PM</option>
                                        <option value="01:00 PM">01:00 PM</option>
                                        <option value="01:30 PM">01:30 PM</option>
                                        <option value="02:00 PM">02:00 PM</option>
                                        <option value="02:30 PM">02:30 PM</option>
                                        <option value="03:00 PM">03:00 PM</option>
                                        <option value="03:30 PM">03:30 PM</option>
                                        <option value="04:00 PM">04:00 PM</option>
                                        <option value="04:30 PM">04:30 PM</option>
                                        <option value="05:00 PM">05:00 PM</option>
                                        <option value="05:30 PM">05:30 PM</option>
                                        <option value="06:00 PM">06:00 PM</option>
                                        <option value="06:30 PM">06:30 PM</option>
                                        <option value="07:00 PM">07:00 PM</option>
                                        <option value="07:30 PM">07:30 PM</option>
                                        <option value="08:00 PM">08:00 PM</option>
                                        <option value="08:30 PM">08:30 PM</option>
                                        <option value="09:00 PM">09:00 PM</option>
                                        <option value="09:30 PM">09:30 PM</option>
                                        <option value="10:00 PM">10:00 PM</option>
                                        <option value="10:30 PM">10:30 PM</option>
                                        <option value="11:00 PM">11:00 PM</option>
                                        <option value="11:30 PM">11:30 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Drop-off date/time end -->

                        <?php
                        if(checkSession()){
                            echo '<input type="submit" class="submit" name="submit" value="গাড়িটি রিজার্ভ করুন" id="checkoutModalLabel">';
                        } else {
                            echo '<input type="submit" class="submit" name="submit" value="continue car reservation" id="checkoutModalLabel">';
                        }
                        ?>
                    </form>

                </div>
            </div>

        </div>
    </div>
</section>
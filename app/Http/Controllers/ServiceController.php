<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class ServiceController extends Controller
{

    //      WeddingController
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = DB::table('services')
                    ->get();
        return view('backend.service.services',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //$servicetypes = DB::table('servicetypes')->get();
        return view('backend.service.addservice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';

        // if(Input::has('name'))
        // {
        //     return "hy";
        //     
       
            
        //     //return $filename;
        // }
        Image::make(Input::file('name'))->save('public/uploads/service/'.$filename);

        DB::table('services')->insert(
        [
            'service_title' => Input::get('service_title'),
            'service_type_id' => 0,
            'service_image' => $filename,
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
        ]
        );
         return redirect('allservices')->with('success', 'New Service Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = DB::table('services')
                    ->where('services.id',$id)
                    ->get();
       
        $service = $services[0];
        //return $services;
        return view('backend.service.editservice',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('services')
            ->where('id', $id)
            ->update([
                    'service_title' => Input::get('service_title'),
                    'service_description' => Input::get('editor1'),
                    'service_status' => 1,
                ]);
            if(Input::file('name'))
            {
                //return 'hy';
                 $filename = time().'.jpg';

                 Image::make(Input::file('name'))->save('public/uploads/service/'.$filename);
                   DB::table('services')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('allservices')->with('success', 'Service Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ser = DB::table('services')->where('id', $id)->delete();
        //$image = $ser
        //if()


        return redirect('allservices')->with('success', 'Selected Service removed Successfully');
    }
}

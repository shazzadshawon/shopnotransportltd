<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    
     
      public function location($ip)
    {
        //$ip = \Request::ip();
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            $reg = explode(" ",$details->region);
            echo "<pre>";
           // print_r(strtolower($reg[0]));
            print_r($details);
            exit;
    }

      public function complain()
    {
       return view('backend.complain');
    }
    
    public function postcomplain()
    {
    if ( !empty(Input::get('sjw_name')) && !empty(Input::get('sjw_email')) && !empty(Input::get('editor')) ){
        $user_name = Input::get('sjw_name');
        $user_email = Input::get('sjw_email');
        $user_message = Input::get('editor');
        $contact_addr = 'complain@shobarjonnoweb.com';
        //$contact_addr = 'complain@shobarjonnoweb.com';
//        $contact_addr = 'contact@kababiabd.com';

        $subject = 'Complain Mail';
//        $message = $user_message.'<br>'.'regards'.'<br>'.$user_name;
        $message_body = "<hr>".
                        "<br/>".
                        "<br/>".
                        $user_message.
                        "<br>".
                        "<hr>".
                        "Name: ".$user_name."<br>".
                        "Email: ".$user_email."<br/>";

        $message = '
                    <style>
                        body {
                            padding: 0;
                            margin: 0;
                        }

                        html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
                        @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
                            *[class="table_width_100"] {
                                width: 96% !important;
                            }
                            *[class="border-right_mob"] {
                                border-right: 1px solid #dddddd;
                            }
                            *[class="mob_100"] {
                                width: 100% !important;
                            }
                            *[class="mob_center"] {
                                text-align: center !important;
                            }
                            *[class="mob_center_bl"] {
                                float: none !important;
                                display: block !important;
                                margin: 0px auto;
                            }
                            .iage_footer a {
                                text-decoration: none;
                                color: #929ca8;
                            }
                            img.mob_display_none {
                                width: 0px !important;
                                height: 0px !important;
                                display: none !important;
                            }
                            img.mob_width_50 {
                                width: 40% !important;
                                height: auto !important;
                            }
                        }
                        .table_width_100 {
                            width: 680px;
                        }
                    </style>
                    <div id="mailsub" class="notification" align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">
                            <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                                <tr><td>                
                                </td></tr>
                                <tr><td align="center" bgcolor="#ffffff">
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                            <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; float:left; width:100%; padding:20px;text-align:center; font-size: 13px;">
                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                    <img src="http://shobarjonnoweb.com/assets/shobarjonnoweb.22bd6958feffc3b2.png" width="250" alt="Metronic" border="0"  /></font></a>
                                        </td>
                                            <td align="right">
                                            </td></tr>
                                        <tr><td align="center" bgcolor="#fbfcfd">
                                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
                                                
                                                <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td></td></tr>
                                                    <tr style="margin-top: 10px;"><td>Dear '.$contact_addr.',</td></tr>
                                                    <tr><td>Asalamualaikum</td></tr>
                                                    <tr><td>
                                                        '.$message_body.'
                                                    </td></tr>
                                                    <tr><td>With best regards,</td></tr>
                                                    <tr><td><strong>'.$user_name.'</strong></td></tr>
                                                </table>
                                            </font>
                                        </td></tr>
                                        <tr><td class="iage_footer" align="center" bgcolor="#ffffff">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr><td align="center" style="padding:20px;flaot:left;width:100%; text-align:center;">
                                                    <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                                        <img src="http://www.basis.org.bd/logo/a3ff7c89595fa36153d8ae1c8ab071a1.png" width="250" alt="Metronic" border="0" style="width: 115px; float: left;" />
                                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5; float: right;">
                                                        2017-2018 &copy; AGV. ALL Rights Reserved.
                                                    </span></font>
                                                </td></tr>
                                            </table>
                                        </td></tr>
                                        <tr><td>
                                        </td></tr>
                                    </table>
                                </td></tr>
                            </table>
                        </table>
                    </div>
                    ';

        $headers = "From: ".$user_email."\r\n";
        $headers .= "MIME-Version: 1.0" . "\r\n";
        //$headers .= "Content-Type: multipart/mixed; boundary=\"\"\r\n\r\n";
        //$headers .= 'Cc: akramul.i@agvcorp.com' . "\r\n";
        //$headers .= 'Cc: '.$contact_addr. "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        //$headers .= "Content-Transfer-Encoding: 7bit\r\n\r\n";

        if ( mail($contact_addr, $subject, $message, $headers) ){
            return redirect()->back()->with('success','Your Complain has been placed successfully');
        } else {
            return redirect()->back()->with('danger','Something went wrong! Please try again');
        }
    }

    }
    
    
    
    
    
    
    public function index(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
        
        
// Event line Chart
  
          $dates = DB::table('item1')->distinct()
          ->where('created_at','>',date("Y-m-d"))
          ->orderby('created_at','asc')->get(['created_at']);
          $dateArray  = array();
          $countArray = array();
          $i=0;

          foreach ($dates as $date) {
            if ($i>9) {
                break;
            }
              $dateArray[] = $date->created_at;
                $date_count = DB::table('events')->where('created_at', $date->created_at )->count();
                $countArray[] = $date_count;
                $i++;
          }
          //return $countArray;
// lineChartTest

            $postjs = app()->chartjs
                     ->name('barChartTest')
                     ->type('line')
                     ->size(['width' => 100, 'height' => 45])
                     ->labels($dateArray)
                     ->datasets([
                         [
                             "label" => "Reservations per day",
                             'backgroundColor' => 'skyblue',
                             'data' => $countArray
                         ]

                   
                     ])
                     ->options([]);
                     

        
        
// visitor donat chart
                   
            $visitor = \DB::table('visitors')->first();
//print_r($visitor);
            $visitorArray = array();
            
            $visitorArray[] = $visitor->barisal;
            $visitorArray[] = $visitor->chittagong;
            $visitorArray[] = $visitor->dhaka;
            $visitorArray[] = $visitor->khulna;
            $visitorArray[] = $visitor->mymensingh;
            $visitorArray[] = $visitor->rajshahi;
            $visitorArray[] = $visitor->rangpur;
            $visitorArray[] = $visitor->sylhet;
//print_r($visitorArray);
//exit;
            $visitorjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('doughnut')
                    ->size(['width' => 400, 'height' => 400])
                    ->labels(['Barisal','Chittagong','Dhaka','Khulna','Mymensingh','Rajshahi','Rangpur', 'Sylhet'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#f7970e', '#00b39a','#ff6664','#4caf50','#03a9f4','#111317','#673ab7','#ff5722','purple'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB'],
                            'data' => $visitorArray
                        ]
                    ])
                    ->options([
                         'legend'=> [
                                'display'=> false,
                                'position'=> 'left',
                                'labels' => [
                                    'fontColor' => 'teal'
                                    ]
                                ]
                        ]);
        
        
       
              
        $app_token = '1966809476921747|XNzsEplX2mALSRn0ePaFqTHY0K0';
        $fullresponse = $fb->get('/250434772134371/feed?fields=from,message,updated_time,comments,shares,reactions&limit=10', $app_token)->getDecodedBody();
        $response = $fullresponse['data'];
        
       
        
        //1966809476921747
        return view('backend.dashboard',compact('postjs','visitorjs','response'));
        //return view('backend.dashboard',compact('postjs','visitorjs'));
    }
    
    
    
    public function all_location()
    {
        $locations = DB::table('locations')->get();
        return view('backend.location.locations',compact('locations'));
    }

    public function addlocation()
    {
        return view('backend.location.addlocation');
    }

    public function storelocation()
    {
        DB::table('locations')->insert(
            [
                'value' => Input::get('value'),
                'created_at' => date('d/m/Y'),

            ]
            );
        return redirect('all_location')->with('success','Location Added Successfully');
    }

  
    public function deletelocation($id)
    {
        DB::table('locations')->where('id',$id)->delete();
        return redirect('all_location')->with('success','Location Deleted Successfully');
    }






//   Reservations

    public function all_reservation()
    {
         $reservations = DB::table('reservations')
         ->leftjoin('item1','reservations.car_id','item1.id')
         ->select('reservations.*','item1.service_title')
         ->get();
        return view('backend.reservation.reservations',compact('reservations'));
    }

    public function mobile_reservation()
    {
         $reservations = DB::table('phone_reservation')
         ->get();
        return view('backend.reservation.mobile_reservation',compact('reservations'));
    }
    public function email_reservation()
    {
         $reservations = DB::table('email_reservation')
         ->get();
        return view('backend.reservation.email_reservation',compact('reservations'));
    }

    public function deletecarreservation($id)
    {
         $reservations = DB::table('reservations')
        ->where('id',$id)->delete();
        return redirect('all_reservation')->with('success', 'Reservation Deleted Successfully');
    }


    public function deleteemailreservation($id)
    {
         $reservations = DB::table('email_reservation')
        ->where('id',$id)->delete();
        return redirect()->back()->with('success', 'Email Reservation Deleted Successfully');
    }


    public function deletemobilereservation($id)
    {
         $reservations = DB::table('phone_reservation')
        ->where('id',$id)->delete();
        return redirect()->back()->with('success', 'Phone Reservation Deleted Successfully');
    }



    public function members()
    {
         $members = DB::table('registrations')
        ->get();
        return view('backend.members',compact('members'));
    }



    public function deletemember($id)
    {
         $registration = DB::table('registrations')
        ->where('id',$id)->delete();
        return redirect()->back()->with('success', 'Member Removed Successfully');
    }




  

  


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = DB::table('reviews')
                    ->get();
        return view('backend.review.reviews',compact('reviews'));
    }



    public function add()
    {
        return view('backend.review.addreview');
    }


    public function store(Request $request)
    {
      
        date_default_timezone_set('Asia/Dhaka');
        DB::table('reviews')->insert(
        [
            'review_title' => Input::get('review_title'),
            'review_title_bn' => Input::get('review_title_bn'),
            
            
            'review_description' => Input::get('editor1'),
            'review_description_bn' => Input::get('editor2'),
            'review_status' => 1,
            'created_at' =>  date('d F, Y'),
                            //date("l jS \of F Y h:i:s A"),
            //'created_at' => date('d/F/Y h:i a', time()),
        ]
        );
        return redirect('reviews')->with('success', 'New Review Added Successfully');
    }


    public function show($id)
    {
        //
    }



    public function edit($id)
    {
        $review = DB::table('reviews')
                    ->where('id',$id)
                    ->first();
       
        //return $reviews;
        return view('backend.review.editreview',compact('review'));
    }



    public function update(Request $request, $id)
    {
        //return Input::all();
         date_default_timezone_set('Asia/Dhaka');
         DB::table('reviews')
            ->where('id', $id)
            ->update([
                    'review_title' => Input::get('review_title'),
                    'review_description' => Input::get('editor1'),
                    'review_title_bn' => Input::get('review_title_bn'),
                    'review_description_bn' => Input::get('editor2'),
                    'review_status' => 1,
                   'created_at' => date('d F, Y'),
                ]);
          
            return redirect('reviews')->with('success', 'review Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('reviews')->where('id', $id)->delete();
      


        return redirect('reviews')->with('success', 'review removed Successfully');
    }
}

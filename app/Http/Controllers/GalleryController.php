<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = DB::table('galleries')
         ->get();
        return view('backend.gallery.galleries',compact('galleries'));
    }

 

    public function add()
    {
        return view('backend.gallery.addgallery');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return Input::all();
        //return Input::file('gallery_image');

        $imgname = Input::get('gallery_image');
        $filename = time().'.jpg';
        $place = Input::get('place');

        Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);

        
       

        DB::table('galleries')->insert(
        [
            'gallery_image' => $filename,
            'gallery_image_status' => 1,
            'folder' => $place,
        ]
        );
         return redirect('galleries')->with('success', 'New Image Uploaded Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gal = DB::table('galleries')
                    ->where('id',$id)
                    ->get();
                    $gallery = $gal[0];
        return view('backend.gallery.editgallery',compact('gallery'));
    }



    public function update(Request $request, $id)
    {
        $place = Input::get('place');
          if(Input::file('gallery_image'))
            {
                //return 'hy';
                 $filename = time().'.jpg';
Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);
                   

        // if($place == 'wedding'){
        //     //return 1;
            
        // }
        // elseif($place == 'events'){
        //     Image::make(Input::file('gallery_image'))->save('public/images/gallery/events/'.$filename);
        // }
        // elseif ($place == 'music') {
        //      Image::make(Input::file('gallery_image'))->save('public/images/gallery/music/'.$filename);
        // }


                 //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);

                   DB::table('galleries')
            ->where('id', $id)
            ->update([
                    
                    'gallery_image' => $filename,
                    'folder' => $place,
                    
                ]);


             //return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
             }
             else{

                   DB::table('galleries')
            ->where('id', $id)
            ->update([
                    
                    
                    'folder' => $place,
                    
                ]);


             }
             return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = DB::table('galleries')
            ->where('id', $id)->get();
 $folder = $image[0]->folder;

            //$img = Image::destroy('public/images/gallery/'.$folder.'/'.$image[0]->gallery_image);

            //$img->destroy();
unlink('public/uploads/gallery/'.$image[0]->gallery_image);

        DB::table('galleries')
            ->where('id', $id)->delete();
        return redirect('galleries')->with('success', ' Image Deleted Successfully');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $services = DB::table('music')->get();
        return view('backend.music.musics',compact('services'));
    }


    public function add()
    {
        
        return view('backend.music.addmusic');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';

        // if(Input::has('name'))
        // {
        //     return "hy";
        //     
       
            
        //     //return $filename;
        // }
        Image::make(Input::file('name'))->save('public/uploads/music/'.$filename);

        DB::table('music')->insert(
        [
            'service_title' => Input::get('service_title'),
           
            'service_image' => $filename,
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
        ]
        );
         return redirect('musics')->with('success', 'New music Event Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = DB::table('music')
                    ->where('id',$id)
                    ->get();
        //$servicetypes = DB::table('int_subcategories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.music.editmusic',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('music')
            ->where('id', $id)
            ->update([
                    'service_title' => Input::get('service_title'),
                    'service_description' => Input::get('editor1'),
                    'service_status' => 1,
                    
                ]);
            if(Input::file('name'))
            {
                //return 'hy';
                 $filename = time().'.jpg';

            Image::make(Input::file('name'))->save('public/uploads/music/'.$filename);
            DB::table('music')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('musics')->with('success', 'music Event Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ser = DB::table('music')->where('id', $id)->delete();
        //$image = $ser
        //if()


        return redirect('musics')->with('success', 'Selected  Service removed Successfully');
    }
}

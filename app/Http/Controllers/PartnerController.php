<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $services = DB::table('partners')->get();
        return view('backend.partner.partners',compact('services'));
    }


    public function add()
    {
        
        return view('backend.partner.addpartner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('logo');
        $filename = time().'.jpg';



        Image::make(Input::file('logo'))->save('public/uploads/partner/'.$filename);

        DB::table('partners')->insert(
        [
            'title' => Input::get('title'),
            'logo' => $filename,
            'created_at' => date('d/m/Y'),
          
        ]
        );
         return redirect('partners')->with('success', 'New Partner Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = DB::table('partners')
                    ->where('id',$id)
                    ->get();
        //$servicetypes = DB::table('int_subcategories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.partner.editpartner',compact('service'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('partners')
            ->where('id', $id)
            ->update([
                    'service_title' => Input::get('service_title'),
                    'service_description' => Input::get('editor1'),
                    'service_status' => 1,
                ]);
            if(Input::file('name'))
            {
                $item = DB::table('partners')
                    ->where('id', $id)->first();
                $image = $item->service_image;

                unlink('public/uploads/partners/'.$image);

                $filename = time().'.jpg';

            Image::make(Input::file('name'))->save('public/uploads/partners/'.$filename);
            DB::table('partners')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('partners')->with('success', 'Story Updated Successfully');

    }



    public function destroy($id)
    {
         $item = DB::table('partners')
                    ->where('id', $id)->first();
                $image = $item->logo;

                unlink('public/uploads/partner/'.$image);

        
        DB::table('partners')->where('id', $id)->delete();


        return redirect('partners')->with('success', 'Selected Partner removed Successfully');
    }

}

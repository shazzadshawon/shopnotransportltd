<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class Item1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $items = DB::table('item1')
                    ->get();
        return view('backend.item1.item1',compact('items'));
    }


    public function add()
    {
        //$int_categories = DB::table('int_categories')->get();
        return view('backend.item1.additem1');
    }



    public function store(Request $request)
    {
       

        if (Input::file('image')) {
            $file = $request->file('image');
           
                 $filename = rand(1,9999999999999).'.jpg';
                 Image::make($file)->save('public/uploads/item1/'.$filename);

                  DB::table('item1')->insert([
                    'service_title' => $request->get('service_title'),
                    'service_title_bn' => $request->get('service_title_bn'),
                    'image_name' => $filename,
                    'service_description' => $request->get('editor1'),
                    'service_description_bn' => $request->get('editor2'),
                    ]);
     return redirect('item1')->with('success', 'New Car Added Successfully');
        }

 return redirect()->back()->with('success', 'Please select an Image');
       
        
    }



    public function show(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    {

          if(!session_id()) {
             session_start();
          }


          
        $item = DB::table('item1')
                ->where('id',$id)->first();
         // $item_images = DB::table('item_images_1')
         //        ->where('item_id',$id)->get();
         //        foreach ($item_images as $key) {
         //            print_r($key);
         //        }



        session(['service_id' => $item->id]);
        session(['service_title' => $item->service_title]);
         
        // if(!session_id()) {
        //     session_start();
        //     //$_SESSION["post_id"] = $cat->id;
        //     session(['wed' => '']);
        //     session(['wed' => $service->id]);
        // }
        
 
      
         $login_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl('http://keyperltd.com/work_callback', ['email','user_events']);
           // ->getLoginUrl('localhost/red_done_n/service_callback', ['email', 'user_events']);
        
         
        //return view('backend.service.singleservice',compact('service','servicetypes','login_link'));

        return view('backend.item1.viewitem1',compact('item','login_link'));
    }



    public function edit($id)
    {
        $services = DB::table('item1')
                    ->where('id',$id)
                    ->get();
        //$cats = DB::table('int_categories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.item1.edititem1',compact('service'));
    }



    public function update(Request $request, $id)
    {
        //return Input::all();
          $car = DB::table('item1')
            ->where('id', $id)
            ->update([
            'service_title' => Input::get('service_title'),
            'service_description' => Input::get('editor1'),
            'service_title_bn' => Input::get('service_title_bn'),
            'service_description_bn' => Input::get('editor2'),
            'service_status' => 1,
            // 'service_sub_cat_id' => '',
                ]);

          if (Input::file('image_name')) {
            //unlink('public/uploads/item1/'.$car->image_name);
            $filename = rand(1,9999999999999).'.jpg';
                 Image::make(Input::file('image_name'))->save('public/uploads/item1/'.$filename);

             DB::table('item1')
            ->where('id', $id)
            ->update([
              'image_name' => $filename,
               ]);
           }




            return redirect('item1')->with('success', ' Car Info Updated Successfully');

    }



    public function destroy($id)
    {
        //   Delete Image
        $item = DB::table('item1')
            ->where('id', $id)->first();
       // $item_images = DB::table('item_images_1')->where('item_id', $id)->get();
       if(!empty($item->image_name))
            {
                 unlink('public/uploads/item1/'.$item->image_name);
            }
        
        DB::table('item1')
            ->where('id', $id)->delete();
           
        
           

         return redirect()->back()->with('success', 'Selected  Item removed Successfully');
    }

  

    // public function storeitem1image(Request $request, $id)
    // {
    //     if (Input::file('images')) {
    //         $files = $request->file('images');
    //         foreach ($files as $file) {
    //              $filename = rand(1,9999999999999).'.jpg';
    //              Image::make($file)->save('public/uploads/item1/'.$filename);

    //               DB::table('item_images_1')->insert([
    //                 'item_id'  => $id,
    //                 'image_name' => $filename,
    //                 ]);
    //         }
    //     }
    //     return redirect()->back()->with('success', 'Image Added Successfully');
    // }


    // public function deleteitem1image($id)
    // {
    //     //   Delete Image
    //     $item = DB::table('item_images_1')->where('id', $id)->first();

       
    //     $image = $item->image_name;
    //     if (!empty($image)) {
    //         unlink('public/uploads/item1/'.$image);
    //     }
        

    //      DB::table('item_images_1')->where('id', $id)->delete();

    //     return redirect()->back()->with('success', 'Selected  Image removed Successfully');
    // }






    

    
    public function work_callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
           
         if(!session_id()) {
             session_start();
         }
         

         $post_id =  session('service_id');
         $post_title =  session('service_title');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //return typeof($accessToken);
          //$accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('1923089044631336'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            //$accessToken = $oAuth2Client->getLongLivedAccessToken('EAABeioWfpIgBAHOc0XTZBwVKb4PZAbhzOfkiZAy3LoZCLPZAu9WlYkrjpEPC75RMhNZAnxLyyztpFRMZCpoyOZC7XgxXKNLMUEtw3ZCi3GqL06gVdQOmdtKL5wTHpDy3Xr7XIVt8Fq6ZBobmOMso49r8iHgJ6AsYVcc6gnu6ZBMO6Jea5eJLsU2rQvZCqs42UZBwmdZChhgSD3zeBZA9QZDZD');
            $accessToken = $oAuth2Client->getLongLivedAccessToken('1966809476921747|XNzsEplX2mALSRn0ePaFqTHY0K0');
                                                                    
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          //echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
         // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://keyperltd.com/service_callback/".$post_id,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/125126468121186/feed', $params);
                  return redirect('/shobarjonnoweb')->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        
        

    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $services = DB::table('customer_care')->get();
        return view('backend.customer.customers',compact('services'));
    }


    public function add()
    {
        
        return view('backend.customer.addcustomer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('image');
        $filename = time().'.jpg';



        Image::make(Input::file('image'))->save('public/uploads/customer/'.$filename);

        DB::table('customer_care')->insert(
        [
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'mobile' => Input::get('mobile'),
            'type' => Input::get('type'),
            'image' => $filename,
            'created_at' => date('d/m/Y'),
          
        ]
        );
         return redirect('customercares')->with('success', 'New Employee Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = DB::table('customer_care')
                    ->where('id',$id)
                    ->get();
        //$servicetypes = DB::table('int_subcategories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.customer.editcustomer',compact('service'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('customer_care')
            ->where('id', $id)
            ->update([
                    'service_title' => Input::get('service_title'),
                    'service_description' => Input::get('editor1'),
                    'service_status' => 1,
                ]);
            if(Input::file('name'))
            {
                $item = DB::table('customers')
                    ->where('id', $id)->first();
                $image = $item->service_image;

                unlink('public/uploads/customers/'.$image);

                $filename = time().'.jpg';

            Image::make(Input::file('name'))->save('public/uploads/customers/'.$filename);
            DB::table('customer_care')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('customers')->with('success', 'Story Updated Successfully');

    }



    public function destroy($id)
    {
         $item = DB::table('customer_care')
                    ->where('id', $id)->first();
                $image = $item->image;

                unlink('public/uploads/customer/'.$image);

        
        DB::table('customer_care')->where('id', $id)->delete();


        return redirect('customercares')->with('success', 'Selected Employee removed Successfully');
    }

}

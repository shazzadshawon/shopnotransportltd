-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2017 at 09:19 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_keyper`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `about_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `about_image`, `about_description`, `created_at`, `updated_at`) VALUES
(4, NULL, 'Corporate Decoration/Stall/Fair', NULL, NULL),
(5, NULL, 'Grand Opening ceremony', NULL, NULL),
(6, NULL, 'Corporate Program & Seminar', NULL, NULL),
(7, NULL, 'Road Show / Rally', NULL, NULL),
(8, NULL, 'Birthday Party / Wedding Program', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(1, 'Story 1', '1501583175.jpg', '<p>Demo Description</p>', NULL, NULL, '1', NULL, NULL),
(2, 'Story 2', '1501652401.jpg', '<p>demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;demo Description&nbsp;</p>', NULL, NULL, '1', NULL, NULL),
(3, 'Story 3', '1501652499.jpg', '<p>gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;gbytg 6yhuju8ij8i dwxdwexe iuklloop&nbsp;</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_phone` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `contact_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_title`, `contact_email`, `contact_phone`, `contact_description`, `contact_image`, `created_at`, `updated_at`) VALUES
(7, 'last', 'Demo@gmail.com2', '0', '3rr3', NULL, '2017-08-06 03:59:41', '2017-08-06 03:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `item1`
--

CREATE TABLE `item1` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `item1`
--

INSERT INTO `item1` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(17, 'Work 1', '', '<p>Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;Data from Admin Panel&nbsp;</p>', NULL, NULL, '1', NULL, NULL),
(18, 'Work 2', '', '<p>Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;Dynamic Data&nbsp;</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item2`
--

CREATE TABLE `item2` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item2`
--

INSERT INTO `item2` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(4, 'Service-1', '', '<p>Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;Dynamic data from backend&nbsp;</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_images_1`
--

CREATE TABLE `item_images_1` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `image_name` longtext COLLATE utf8_unicode_ci,
  `image_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_images_1`
--

INSERT INTO `item_images_1` (`id`, `item_id`, `image_name`, `image_status`, `created_at`, `updated_at`) VALUES
(45, 17, '253402563.jpg', NULL, NULL, NULL),
(46, 17, '1197647424.jpg', NULL, NULL, NULL),
(47, 17, '1194474369.jpg', NULL, NULL, NULL),
(48, 17, '663529930.jpg', NULL, NULL, NULL),
(49, 18, '1016381651.jpg', NULL, NULL, NULL),
(50, 18, '1034576509.jpg', NULL, NULL, NULL),
(51, 18, '447240059.jpg', NULL, NULL, NULL),
(52, 18, '1062129871.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_images_2`
--

CREATE TABLE `item_images_2` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `image_name` longtext COLLATE utf8_unicode_ci,
  `image_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_images_2`
--

INSERT INTO `item_images_2` (`id`, `item_id`, `image_name`, `image_status`, `created_at`, `updated_at`) VALUES
(38, 4, '1113782382.jpg', NULL, NULL, NULL),
(39, 4, '772578584.jpg', NULL, NULL, NULL),
(40, 4, '906128040.jpg', NULL, NULL, NULL),
(41, 4, '1211745427.jpg', NULL, NULL, NULL),
(42, 4, '1271069501.jpg', NULL, NULL, NULL),
(43, 4, '1218412858.jpg', NULL, NULL, NULL),
(44, 4, '887330703.jpg', NULL, NULL, NULL),
(45, 4, '232195311.jpg', NULL, NULL, NULL),
(46, 4, '531225597.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2017_07_17_113813_create_categories_table', 1),
(16, '2017_07_17_113838_create_subcategories_table', 1),
(17, '2017_07_17_113916_create_sliders_table', 1),
(18, '2017_07_17_113940_create_events_table', 1),
(19, '2017_07_17_113959_create_teams_table', 1),
(20, '2017_07_17_114018_create_reviews_table', 1),
(21, '2017_07_17_114045_create_galleries_table', 1),
(22, '2017_07_17_114101_create_abouts_table', 1),
(23, '2017_07_17_114121_create_services_table', 1),
(24, '2017_07_18_035856_create_eventcategories_table', 1),
(27, '2017_07_18_042138_create_packages_table', 2),
(28, '2017_07_18_042154_create_packagecategories_table', 2),
(29, '2017_07_18_055256_create_servicetypes_table', 3),
(30, '2014_02_09_225721_create_visitor_registry', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `review_title`, `review_description`, `review_image`, `review_status`, `created_at`, `updated_at`) VALUES
(8, 'Review 4', '<p>This is a demo review 4</p>', '1500713667.jpg', '1', '22 July, 2017', NULL),
(9, 'Review 2', '<p>Demo&nbsp;review 2</p>', '1500713867.jpg', '1', '22 July, 2017', NULL),
(10, 'Review 1', '<p>trbtyhnyun 65u6n5u5u</p>', '1500714088.jpg', '1', '22 July, 2017', NULL),
(11, 'Review 3', '<p>tbt 6yh67 65yuh65yun 656un6un 77jmuyjmuy hjujymnu drfewr sdcse ki,lk,il&nbsp;</p>', '1500714250.jpg', '1', '22 July, 2017', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(15, 'Service 1', '1500523815.jpg', '<p>uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;uj6h7u vedv&nbsp;</p>', 3, 3, '1', NULL, NULL),
(16, 'Birthday Stage & Balloon Decoration', '1500634536.jpg', '<p>ed Elegance Event&rsquo;s Balloon Decorating Service is designed to help you turn your party into an unforgettable event. Whether you are entertaining a few children at home, organising a dinner at a restaurant or hotel, hosting a product launch for thousands, balloon decorations are guaranteed to put your guests in the party mood.</p>\r\n\r\n<p>Over the years we have specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Decorating Service, please contact us.</p>\r\n\r\n<p>Starting From 8000 bdt</p>', 3, 2, '1', NULL, NULL),
(17, 'Photography & Cinematography', '1500633934.jpg', '<p>Khandany (Co-partner of Red Elegance) is a key photography solution for your elegant programs such as Weddings, Corporate events, Product photography and enjoyable family moments. We have a bunch of efficient , dedicated and highly experienced photographers. We want to give you the very best according to your budget taste and expectation. We believe our pursuit is to photograph your beautiful moments to keep them remarkable forever.</p>\r\n\r\n<p>Photography packages</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1>Khandany 101</h1>\r\n\r\n<ul>\r\n	<li>Price: 5500 (Promotional Offer)</li>\r\n	<li>Number of Photographer: 01 (One senior photographer)</li>\r\n	<li>Duration: 4.30 hour (1 Event)</li>\r\n	<li>Number of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, and group photo)</li>\r\n	<li>Light setup: Umbrella portrait Light</li>\r\n	<li>Delivery: Soft copies will be delivered in a nicely illustrated DVD</li>\r\n	<li>Prints : 4R=100 copy (Matt or Glossy paper)</li>\r\n</ul>\r\n\r\n<h1>Khandany 102</h1>\r\n\r\n<ul>\r\n	<li>Price: 8000 (Promotional Offer)</li>\r\n	<li>Number of Photographer: 02 (Two senior photographer )</li>\r\n	<li>Duration: 4.30 hour (1 Event)</li>\r\n	<li>Number of Capture: Unlimited (The photographer will cover decoration, venue, portraits, couple shots, family photo, and group photo)</li>\r\n	<li>Light setup: Umbrella portrait Light</li>\r\n	<li>Delivery: Soft copies will be delivered in a nicely illustrated DVD</li>\r\n	<li>Prints : 4R=120 copy (Matt or Glossy paper), 12L=1 copy</li>\r\n</ul>', NULL, 1, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_status`, `created_at`, `updated_at`) VALUES
(3, '1501667040.jpg', 'Slider 1', 'Demo Subitle 2', '1', NULL, NULL),
(5, '1501667051.jpg', 'Slider 2', 'subtitle 2', '1', NULL, NULL),
(6, '1501667066.jpg', 'Slider 3', 'fsfs', '1', NULL, NULL),
(7, '1501667081.jpg', 'Slider 4', 'rfwre', '1', NULL, NULL),
(8, '1501646227.jpg', 'New', 'dwed', '1', NULL, NULL),
(9, '1501646673.jpg', 'ujmyu', 'nyjy', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_title`, `team_description`, `team_contact`, `team_image`, `team_status`, `created_at`, `updated_at`) VALUES
(4, 'member 2', 'Executive', NULL, '1501665095.jpg', '1', NULL, NULL),
(6, 'member 4', 'Executive', NULL, '1501665068.jpg', '1', NULL, NULL),
(11, 'Member 7', 'Executive', NULL, '1501664869.jpg', '1', NULL, NULL),
(13, 'Member-5', 'Executive', NULL, '1501665058.jpg', '1', NULL, NULL),
(14, 'Member-7', 'Executive', NULL, '1501665044.jpg', '1', NULL, NULL),
(15, 'member 1', 'Executive', NULL, '1501665117.jpg', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$36UNCJVp0rA0UURa3leOiebSOKhgqjAticgs9BquGRH1uEv6LB/SS', 'pDRuYudNASGLBFjGOJ0SY4dkZCdoGxwu61FqiGoZ9E3hcMXkOCV03AXCi9LL', NULL, NULL),
(2, 'Admin AGV', 'admin@agvcorp.biz', '$2y$10$IhzYjNrAxUQY6GlO4ZiAzuUtwquN7a.UB1ly2sA.QrFRESp6betX6', NULL, NULL, NULL),
(3, 'User', 'user@gmail.com', '$2y$10$.rSw4TumCpfMHdaVw7808u9quytqWV5v3MzZ06clWb9oZPc67cQu2', 'QeVAUwNyWhgQYmuv6ZG4COlbNc1ARlEmqJ1TTLUh7bkXUiBb3RilZuBkrz6j', NULL, NULL),
(4, 'Akash', 'akash@gmail.com', '$2y$10$yVMVbg/0ERbvhNQEiVI8TOVgUBa7sGAMYqzXPTbMw7UH.LuFvv0NS', NULL, NULL, NULL),
(7, 'Keyper Admin', 'admin@keyperltd.com', '$2y$10$8eCh8Gng3RBCzRWkT9.oue0d2hhP3LSdmHUbxJIVvfbfuGTeRdbVS', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_name`, `status`, `created_at`, `updated_at`) VALUES
(1, '78097.mp4', 1, '2017-08-03 10:08:44', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item1`
--
ALTER TABLE `item1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item2`
--
ALTER TABLE `item2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_images_1`
--
ALTER TABLE `item_images_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_images_2`
--
ALTER TABLE `item_images_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `item1`
--
ALTER TABLE `item1`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `item2`
--
ALTER TABLE `item2`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_images_1`
--
ALTER TABLE `item_images_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `item_images_2`
--
ALTER TABLE `item_images_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
